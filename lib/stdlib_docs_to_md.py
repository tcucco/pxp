import re
import sys
from collections import defaultdict

sys.path.insert(0, ".")

from pxp.stdlib import global_scope
from pxp.function import _Undefined

def prepare_doc(docstr):
  head, *body = docstr.strip().split("\n\n")
  head = re.sub(r"\s+", " ", head.strip())
  body = [re.sub(r"\s+", " ", line.strip()) for line in body]

  lines = ["> {0}".format(head)]

  if body:
    # return "{head}\n\n{body}".format(head=head, body="\n".join(body))
    lines += [""] + body

  return "\n> ".join(lines)


def arg_to_str(fa):
  if fa.default is _Undefined:
    return "**{0}** *{1}*".format(fa.type, fa.name)
  elif fa.default is None:
    return "**{0}** *{1}*=null".format(fa.type, fa.name)
  else:
    return "**{0}** *{1}*={2}".format(fa.type, fa.name, fa.default)


def md_esc(s):
  return re.sub(r"(\*|_)", r"\\\1", s)

def fn_to_str(fn):
    return "{name}({args}) → **{return_type}**".format(return_type=fn.return_type,
                                                       name=md_esc(fn.name),
                                                       args=", ".join(arg_to_str(arg) for arg in fn.args))


def print_fn(fn):
  print(fn_to_str(fn))
  print()
  print(prepare_doc(fn.doc))
  print()


canonical_sigs = {val.signatures[0] for _, val in global_scope.functions.functions.items()}

operators = []
groups = defaultdict(list)

for sig in sorted(canonical_sigs):
  fn = global_scope.functions[sig]

  if fn.name.startswith("operator"):
    operators.append(((fn.args[0].type, fn.name), fn))
  elif fn.name.find(".") >= 0:
    namespace = fn.name.split(".", 1)[0]
    groups[namespace].append(fn)
  else:
    groups["general"].append(fn)

print("### Operators")
for sk, fn in sorted(operators, key=lambda t: t[0]):
  print_fn(fn)
for group_name in sorted(groups.keys()):
  print("### {0}".format(group_name.title()))
  for fn in groups[group_name]:
    print_fn(fn)
