from .test_compiler import CompilerTestCase
from .test_interpreter import InterpreterTestCase
from .test_stdlib import StdlibTestCase
