import math
from decimal import Decimal as D
from unittest import TestCase

from pxp.exception import FunctionError
from pxp.stdlib import global_scope
from pxp.interpreter import Resolver
from pxp.stdlib.types import number_t as N, string_t as S, boolean_t as B


class StdlibTestCase(TestCase):
  def setUp(self):
    self.scope = global_scope
    self.resolver = Resolver(self.scope)

  def _call(self, signature, *args):
    return self.scope.get_function(signature)(self.resolver, *args)

  def _assert_eq(self, value, signature, *args):
    self.assertEqual(self._call(signature, *args), value)

  def _assert_near(self, value, signature, *args):
    self.assertAlmostEqual(self._call(signature, *args), value)

  def _assert_true(self, signature, *args):
    self._assert_eq(True, signature, *args)

  def _assert_false(self, signature, *args):
    self._assert_eq(False, signature, *args)

  def test_general_functions(self):
    self._assert_eq(D(1), ("if", B, N, N), True, 1, 2)
    self._assert_eq(D(2), ("if", B, N, N), False, 1, 2)
    self._assert_eq("a", ("if", B, S, S), True, "a", "b")
    self._assert_eq("b", ("if", B, S, S), False, "a", "b")
    self._assert_eq(True, ("if", B, B, B), True, True, False)
    self._assert_eq(False, ("if", B, B, B), False, True, False)

    self._assert_eq(True, ("is_num", S), "3.14159")
    self._assert_eq(False, ("is_num", S), "Lord Voldemort")

    self._assert_eq(D("42.4242"), ("to_num", S), "42.4242")

    self._assert_eq("42.4242", ("to_str", N), D("42.4242"))
    self._assert_eq("Minerva McGonagall", ("to_str", S), "Minerva McGonagall")
    self._assert_eq("True", ("to_str", B), True)

    self._assert_eq(True, ("to_bool", S), "true")
    self._assert_eq(False, ("to_bool", S), "false")
    with self.assertRaises(FunctionError):
        self._call(("to_bool", S), "tru")

    self._assert_eq(True, ("to_bool", N), D(1))
    self._assert_eq(False, ("to_bool", N), D(0))

    self._assert_eq(False, ("is_null", S), "Hi")
    self._assert_eq(True, ("is_null", S), None)
    self._assert_eq(False, ("is_null", N), D("50"))
    self._assert_eq(True, ("is_null", N), None)
    self._assert_eq(False, ("is_null", B), False)
    self._assert_eq(True, ("is_null", B), None)

  def test_math_functions(self):
    self._assert_eq(D("42"), ("math.abs", N), D("-42"))
    self._assert_eq(D("42"), ("math.abs", N), D("42"))

    self._assert_eq(D("42"), ("math.ceil", N), D("41.25"))
    self._assert_eq(D("42"), ("math.ceil", N), D("42.0"))

    self._assert_eq(D("1.0"), ("math.cos", N), D("0"))
    self._assert_near(D("0.0"), ("math.cos", N), D(math.radians(90)))

    self._assert_near(D("90"), ("math.degrees", N), D(math.pi / 2.0))

    self._assert_eq(D("42"), ("math.floor", N), D("42.99"))
    self._assert_eq(D("42"), ("math.floor", N), D("42"))

    self._assert_near(D("2"), ("math.log", N), D(math.e ** 2))
    self._assert_near(D("2"), ("math.log", N, N), D("100"), D("10"))

    self._assert_near(D("3"), ("math.log10", N), D("1000"))

    self._assert_near(D("4"), ("math.log2", N), D("16"))

    self._assert_eq(D("8"), ("math.pow", N, N), D("2"), D("3"))

    self._assert_near(D(math.pi), ("math.radians", N), D("180"))

    self._assert_near(D("4"), ("math.root", N, N), D("64"), D("3"))

    self._assert_eq(D("42"), ("math.round", N), D("41.5000"))
    self._assert_eq(D("41.6"), ("math.round", N, N), D("41.59"), D("1"))

    self._assert_eq(D("0.0"), ("math.sin", N), D("0"))
    self._assert_near(D("1.0"), ("math.sin", N), D(math.radians(90)))

    self._assert_eq(D("2"), ("math.sqrt", N), D("4"))

    self._assert_near(D("1.0"), ("math.tan", N), D(math.pi / 4))

  def test_operator_numeric_functions(self):
    n10 = D("10")
    n8 = D("8")

    self._assert_eq(D("18"), ("operator+", N, N), n10, n8)

    self._assert_eq(D("2"), ("operator-", N, N), n10, n8)

    self._assert_eq(D("80"), ("operator*", N, N), n10, n8)

    self._assert_eq(D(10 / 8), ("operator/", N, N), n10, n8)

    self._assert_eq(D("2"), ("operator%", N, N), n10, n8)

    self._assert_eq(D("100000000"), ("operator^", N, N), n10, n8)

    self._assert_eq(n10, ("operator?", N, N), n10, n8)
    self._assert_eq(n8, ("operator?", N, N), None, n8)

    self._assert_true(("operator=", N, N), n10, n10)
    self._assert_false(("operator=", N, N), n10, n8)

    self._assert_true(("operator!=", N, N), n10, n8)
    self._assert_false(("operator!=", N, N), n10, n10)

    self._assert_true(("operator>=", N, N), n10, n8)
    self._assert_true(("operator>=", N, N), n10, n10)
    self._assert_false(("operator>=", N, N), n8, n10)

    self._assert_false(("operator<=", N, N), n10, n8)
    self._assert_true(("operator<=", N, N), n10, n10)
    self._assert_true(("operator<=", N, N), n8, n10)

    self._assert_true(("operator>", N, N), n10, n8)
    self._assert_false(("operator>", N, N), n10, n10)
    self._assert_false(("operator>", N, N), n8, n10)

    self._assert_false(("operator<", N, N), n10, n8)
    self._assert_false(("operator<", N, N), n10, n10)
    self._assert_true(("operator<", N, N), n8, n10)

  def test_operator_string_functions(self):
    sl = "Lord"
    sv = "Voldemort"

    self._assert_eq("LordVoldemort", ("operator+", S, S), sl, sv)

    self._assert_eq(sl, ("operator?", N, N), sl, sv)
    self._assert_eq(sv, ("operator?", N, N), None, sv)

    self._assert_true(("operator=", S, S), sl, sl)
    self._assert_false(("operator=", S, S), sl, sv)

    self._assert_false(("operator!=", S, S), sl, sl)
    self._assert_true(("operator!=", S, S), sl, sv)

    self._assert_false(("operator>=", S, S), sl, sv)
    self._assert_true(("operator>=", S, S), sl, sl)
    self._assert_true(("operator>=", S, S), sv, sl)

    self._assert_true(("operator<=", S, S), sl, sv)
    self._assert_true(("operator<=", S, S), sl, sl)
    self._assert_false(("operator<=", S, S), sv, sl)

    self._assert_false(("operator>", S, S), sl, sv)
    self._assert_false(("operator>", S, S), sl, sl)
    self._assert_true(("operator>", S, S), sv, sl)

    self._assert_true(("operator<", S, S), sl, sv)
    self._assert_false(("operator<", S, S), sl, sl)
    self._assert_false(("operator<", S, S), sv, sl)

  def test_operator_boolean_functions(self):
    t = True
    f = False

    self._assert_eq(t, ("operator?", B, B), t, f)
    self._assert_eq(f, ("operator?", B, B), None, f)

    self._assert_true(("operator=", B, B), t, t)
    self._assert_false(("operator=", B, B), t, f)

    self._assert_false(("operator!=", B, B), t, t)
    self._assert_true(("operator!=", B, B), t, f)

    self._assert_eq(f, ("operatorunary!", B), t)
    self._assert_eq(t, ("operatorunary!", B), f)

    self._assert_eq(t, ("operator&", B, B), t, t)
    self._assert_eq(f, ("operator&", B, B), t, f)
    self._assert_eq(f, ("operator&", B, B), f, t)
    self._assert_eq(f, ("operator&", B, B), f, f)

    self._assert_eq(t, ("operator|", B, B), t, t)
    self._assert_eq(t, ("operator|", B, B), t, f)
    self._assert_eq(t, ("operator|", B, B), f, t)
    self._assert_eq(f, ("operator|", B, B), f, f)

  def test_string_functions(self):
    sa = "Albus"
    sd = "Dumbledore"

    self._assert_true(("str.endswith", S, S), sd, "dore")
    self._assert_false(("str.endswith", S, S), sd, "mort")

    self._assert_eq(5, ("str.find", S, S), sd, "e")
    self._assert_eq(9, ("str.find", S, S, N), sd, "e", 6)
    self._assert_eq(-1, ("str.find", S, S, N, N), sd, "e", 6, 9)
    self._assert_eq(-1, ("str.find", S, S), sd, "Marvolo")

    self._assert_eq(5, ("str.len", S), sa)

    self._assert_eq("albus", ("str.lower", S), sa)

    self._assert_eq("DumblEdorE", ("str.replace", S, S, S), sd, "e", "E")
    self._assert_eq("DumblEdore", ("str.replace", S, S, S, N), sd, "e", "E", D("1"))

    self._assert_eq(sa, ("str.slice", S), sa)
    self._assert_eq("bus", ("str.slice", S, N), sa, D("2"))
    self._assert_eq("lbu", ("str.slice", S, N, N), sa, D("1"), D("4"))

    self._assert_true(("str.startswith", S, S), sd, "Dum")
    self._assert_false(("str.startswith", S, S), sd, "Al")

    self._assert_eq(sa, ("str.strip", S), " Albus     ")

    self._assert_eq("ALBUS", ("str.upper", S), sa)
