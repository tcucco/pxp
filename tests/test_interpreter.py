from decimal import Decimal as D
from unittest import TestCase, skip

from pxp.exception import RuntimeError
from pxp.interpreter import Interpreter
from pxp.scope import DictScope


class InterpreterTestCase(TestCase):
  def test_undefined_variable(self):
    source = """
    #extern Number amount
    return amount * 10;
    """
    with self.assertRaisesRegex(RuntimeError, r"^Undefined variable amount") as cm:
      Interpreter().interpret(source)

  def test_misdefined_variable(self):
    source = """
    #extern Number amount
    return amount + 10;
    """
    scope = DictScope(dict(amount="Trey"))
    with self.assertRaises(RuntimeError) as cm:
      Interpreter(scope).interpret(source)

  def test_divide_by_zero(self):
    source = "10 / 0"
    with self.assertRaisesRegex(RuntimeError, r"^Divide by 0") as cm:
      Interpreter().interpret(source)

  def test_modulus_by_zero(self):
    source = "10 % 0"
    with self.assertRaisesRegex(RuntimeError, r"^Divide by 0") as cm:
      Interpreter().interpret(source)

  def test_non_exhaustive_switch(self):
    source = """
    /* a comment */
    /* another
       comment */
    a <- 10;
    switch a
    when 1: "1"
    when 2: "2"
    """
    with self.assertRaisesRegex(RuntimeError, r"^Non-exhaustive switch") as cm:
      Interpreter().interpret(source)

  def test_exception_throwing_methods(self):
    # an operator that doesn't accept None

    source1 = "to_num('abc')"
    with self.assertRaisesRegex(RuntimeError, r"^Unable to parse string as a Number") as cm:
      Interpreter().interpret(source1)

    source2 = "math.log(10, 0)"
    with self.assertRaisesRegex(RuntimeError, r"^Invalid log base") as cm:
      Interpreter().interpret(source2)

    source3 = """
    #extern Number value
    value + 10
    """
    with self.assertRaisesRegex(RuntimeError, r"^Unexpected null value") as cm:
      Interpreter(DictScope({"value": None})).interpret(source3)

  def test_operators(self):
    tests = [
      ("5 - 3 + 2", D(4)),
      ("5 - (3 + 2)", D(0)),
      ("8 + 2 * 4", D(16)),
      ("(8 + 2) * 4", D(40)),
      ("10 / 2 * 4", D(20)),
      ("10 * 2 / 4", D(5)),
      ("2 ^ 3 ^ 2", D(512)),
      ("(2 ^ 3) ^ 2", D(64)),
      ("3 * 8 - 2 <= 4 / 2 * 100", True),
      ("z ? 3 + x ? 9", D(13)),
      ("false | false & true | true", True),
      ("(false | false) & (true | false)", False),
      ("!true", False),
      ("!!true", True),
      ("5+-3", D(2)),
      ("-x", D(-10)),
      ("-math.sqrt(4)", D(-2)),
      ("--3", D(3)),
      ("[operator+](3, 4)", D(7)),
      ("str.upper([first name])", "TREY")
    ]

    scope = DictScope({"x": 10, "y": -10, "z": None, "first name": "Trey"})
    externs = "#extern Number x\n" \
              "#extern Number y\n" \
              "#extern Number z\n" \
              "#extern String first name\n"
    interpreter = Interpreter(scope)

    for source, expected in tests:
      result = interpreter.interpret(externs + source)
      # print(source, " -> ", result)
      self.assertEqual(result, expected)
