from decimal import Decimal
from unittest import TestCase

from pyebnf.primitive import pprint as pprint_pt

from pxp.ast import pprint as pprint_ast
from pxp.compiler import Compiler
from pxp.exception import CompilerError
from pxp.interpreter import Interpreter, Resolver
from pxp.scope import DictScope
from pxp.stdlib import global_scope


class CompilerTestCase(TestCase):
  def _test_compile_source(self, source, expected_value, *, show_source=False, show_ptree=False,
                           show_ast=False, show_compiled=False, show_value=False):
    if show_source:
      print("The Source:")
      print(source)

    compiler = Compiler(source)

    if show_ptree:
      print()
      print("The Parse Tree:")
      pprint_pt(compiler.parse_tree)

    if show_ast:
      print()
      print("The Abstract Syntax Tree (AST):")
      pprint_ast(compiler.abstract_syntax_tree)

    program = compiler.compile()
    _, ptype, assns, rstmt = program

    if show_compiled:
      print()
      print("The Compiled Code ({0}):".format(ptype))
      for line in assns:
        print(line)
      print(rstmt)

    value = Interpreter().execute(program)
    if show_value:
      print()
      print("Value:")
      print(value)

    self.assertEqual(value, expected_value)

  def test_compile_source(self):
    source1 = """
    a <- 4;
    b <- math.pi / a;
    c <- d ^ e;
    return math.round(math.tan(b)) * 10;
    """

    source2 = """
    a <- if b < 10:
             if b < 5: 5 - b
             else: 10 - b
         elif b = 10: 10
         else: b - 10;
    b <- 7;
    return a;
    """

    source3 = """
    a <- switch b
         when 1: "1"
         when 2: "2"
         when 3: "3"
         else: "Something Big!";
    b <- 40;
    return a;
    """

    source4 = """
    date <- '2015-01-01T09:00:00-06:00';
    pivot <- str.find(date, 'T');
    if pivot = -1: date
    else: str.slice(date, pivot + 1)
    """

    self._test_compile_source(source1, Decimal("10"))
    self._test_compile_source(source2, Decimal("3"))
    self._test_compile_source(source3, "Something Big!")
    self._test_compile_source(source4, "09:00:00-06:00")

  def test_circular_reference(self):
    source = """
    a <- b * 3;
    b <- c / 4;
    c <- 100 - a;
    return a ^ 2;
    """

    with self.assertRaisesRegex(CompilerError, "^Circular reference: ") as cm:
      compiler = Compiler(source)
      program = compiler.abstract_syntax_tree.to_tuple()

  def test_bad_parsing(self):
    source1 = "3 *"
    with self.assertRaisesRegex(CompilerError, "^Failed to parse input ") as cm:
      Interpreter().interpret(source1)

    source2 = "a b <- 3;"
    with self.assertRaisesRegex(CompilerError, "^Failed to parse input ") as cm:
      Interpreter().interpret(source2)

    source3 = "a <- 4 2;"
    with self.assertRaisesRegex(CompilerError, "^Failed to parse input ") as cm:
      Interpreter().interpret(source3)

  def test_bad_directive(self):
    source = """
    #exter String name
    return 40 + 2;
    """

    with self.assertRaisesRegex(CompilerError, "^Unknown directive: #exter") as cm:
      Interpreter().interpret(source)

  def test_directive_extern_bad(self):
    source1 = """
    #extern Num value
    return value + 8;
    """
    with self.assertRaisesRegex(CompilerError, "^Unknown type Num for #extern directive") as cm:
      Interpreter().interpret(source1)

    source2 = """
    #extern Number
    return value + 8;
    """
    with self.assertRaisesRegex(CompilerError, "^Directive #extern expects exactly 2 arguments") as cm:
      Interpreter().interpret(source2)

  def test_directive_extern_good(self):
    source = """
    #extern Number amount
    mult <- 10;
    return amount * mult;
    """

    scope = DictScope(dict(amount=Decimal(5)))
    self.assertEqual(Decimal(50), Interpreter(scope).interpret(source))

  def test_undefined_variable(self):
    source = """
    return a * 10;
    """

    with self.assertRaisesRegex(CompilerError, "^Undefined variable a") as cm:
      Interpreter().interpret(source)

  def test_bad_operator(self):
    source = """
    'abc' + 3
"""

    with self.assertRaisesRegex(CompilerError, r"^Undefined function \('operator\+', 'String', 'Number'\)") as cm:
      Interpreter().interpret(source)

  def test_bad_function_call(self):
    source = """
    a <- 16;
    return math.sqr(a);
    """

    with self.assertRaisesRegex(CompilerError, r"^Undefined function \('math.sqr', 'Number'\)") as cm:
      Interpreter().interpret(source)

  def test_bad_branch(self):
    source1 = """
    if (3 + 5): 7 else: 8
    """

    with self.assertRaisesRegex(CompilerError, r"^Branch predicate is not Boolean type") as cm:
      Interpreter().interpret(source1)

    source2 = """
    a <- 5;
    if a < 10: 10
    else: "20";
    """

    with self.assertRaisesRegex(CompilerError, r"^Not all branches have the same return type") as cm:
      Interpreter().interpret(source2)

  def test_bad_switch(self):
    source1 = """
    b <- switch a
         when 1: "1"
         when 2: "2"
         when 3: "3"
         when 4: 4;
    a <- 3;
    b
    """

    with self.assertRaisesRegex(CompilerError, r"^Switch case return type doesn't match others") as cm:
      Interpreter().interpret(source1)

    source2 = """
    b <- switch a
         when 1: "1"
         when 2: "2";
    a <- "1";
    b
    """

    with self.assertRaisesRegex(CompilerError, r"^Switch case predicate type doesn't match subject type") as cm:
      Interpreter().interpret(source2)
